This is a simple python class to plot Frohlich tringular plots.

based on:

Frohlich, C. (1992). Triangle diagrams: ternary graphs to display similarity and diversity of earthquake focal mechanisms. Physics of the Earth and Planetary Interiors, 75(1-3), 193–198. http://doi.org/10.1016/0031-9201(92)90130-N