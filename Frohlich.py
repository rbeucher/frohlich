from functools import partial
from math import radians, cos, sin, atan2, degrees
import numpy as np
import itertools
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.colors import LinearSegmentedColormap


def project_point(S1, S2, S3):
    s1 = radians(S1)
    s2 = radians(S2)
    s3 = radians(S3)
    
    censin = sin(1.0/np.sqrt(3.0))
    cencos = cos(1.0/np.sqrt(3.0))

    phi = atan2(sin(s1), sin(s3)) - (np.pi / 4.0)
    denom = (censin*sin(s2) + cencos*cos(s2)*cos(phi))
    
    h = (cos(s2)*sin(phi)) / denom
    v = (cencos*sin(s2)-censin*cos(s2)*cos(phi))/denom
    return h,v

def line(angle, val="S1"):    
    angle_radians = radians(angle)
    
    if val is "S1":
        S1 = np.repeat(angle_radians,301)
        sinS1 = np.sin(S1)**2
        sinS2 = np.linspace(-0, 1.0, 301)*(1.0-np.sin(angle_radians)**2)
        sinS3 = 1.0 - (sinS1 + sinS2)
    
        S1 = np.degrees(S1)
        S2 = np.degrees(np.arcsin(np.sqrt(sinS2)))
        S3 = np.degrees(np.arcsin(np.sqrt(sinS3)))
        
    elif val is "S2":
        S2 = np.repeat(angle_radians,301)
        sinS2 = np.sin(S2)**2
        sinS1 = np.linspace(0, 1.0, 301)*(1.0-np.sin(angle_radians)**2)
        sinS3 = 1.0 - (sinS1 + sinS2)
    
        S2 = np.degrees(S2)
        S1 = np.degrees(np.arcsin(np.sqrt(sinS1)))
        S3 = np.degrees(np.arcsin(np.sqrt(sinS3)))
              
    elif val is "S3":
        S3 = np.repeat(angle_radians,301)
        sinS3 = np.sin(S3)**2
        sinS2 = np.linspace(0, 1.0, 301)*(1.0-np.sin(angle_radians)**2)
        sinS1 = 1.0 - (sinS2 + sinS3)
    
        S3 = np.degrees(S3)
        S2 = np.degrees(np.arcsin(np.sqrt(sinS2)))
        S1 = np.degrees(np.arcsin(np.sqrt(sinS1)))
                        
        
    return zip(S1,S2,S3)
    
def plotline(angle, val, color="black", linewidth=0.5, **kwargs):
    A = line(angle, val)
    pts = [project_point(*pt) for pt in list(A)]
    x = [x for (x,y) in pts]
    y = [y for (x,y) in pts]
    plt.plot(x,y, color, linewidth=linewidth, **kwargs)
    
def plotband(angle1, angle2, val, color="black", linewidth=0.5, **kwargs):
    A = line(max(angle1-0.5,0), val)
    pts = [project_point(*pt) for pt in list(A)]
    x1 = [x for (x,y) in pts]
    y1 = [y for (x,y) in pts]
    
    B = line(min(angle2+0.5,90), val)
    pts = [project_point(*pt) for pt in list(B)]
    x2 = [x for (x,y) in pts]
    y2 = [y for (x,y) in pts]
    
    A = list(zip(x1,y1))
    B = list(zip(x2,y2))
    B.reverse()
    P = Polygon(np.array(A+B), color=color, linewidth=0., alpha=0.6)
    ax = plt.gca()
    ax.add_patch(P)
    
def plotblue():
    vals = np.linspace(0,90,100)
    A = vals[:-1]
    B = vals[1:]
    cmap = cm.Blues
    
    cdict = {'red':   ((0.0,  0.0, 0.0),
                       (1.0,  0.0, 0.0)),

         'green': ((0.0,  0.0, 0.0),
                   (1.0,  0.0, 0.0)),

         'blue':  ((0.0,  0.0, 0.0),
                   (1.0,  1.0, 1.0))}
    
    blue_cmap = LinearSegmentedColormap('BlueCmap', cdict)
    
    colors = blue_cmap(np.linspace(0, 1, len(A)))
    for i in range(0,len(A)):
        plotband(A[i],B[i], val="S1", color=colors[i])
    return 0

def plotred():
    vals = np.linspace(0,90,100)
    A = vals[:-1]
    B = vals[1:]
    cmap = cm.Blues
    
    cdict = {'red':   ((0.0,  0.0, 0.0),
                       (1.0,  1.0, 1.0)),

         'green': ((0.0,  0.0, 0.0),
                   (1.0,  0.0, 0.0)),

         'blue':  ((0.0,  0.0, 0.0),
                   (1.0,  0.0, 0.0))}
    
    red_cmap = LinearSegmentedColormap('RedCmap', cdict)
    
    colors = red_cmap(np.linspace(0, 1, len(A)))
    for i in range(0,len(A)):
        plotband(A[i],B[i], val="S3", color=colors[i])
    return 0

def plotgreen():
    vals = np.linspace(0,90,100)
    A = vals[:-1]
    B = vals[1:]
    cmap = cm.Blues
    
    cdict = {'red':   ((0.0,  0.0, 0.0),
                       (1.0,  0.0, 0.0)),

         'green': ((0.0,  0.0, 0.0),
                   (1.0,  1.0, 1.0)),

         'blue':  ((0.0,  0.0, 0.0),
                   (1.0,  0.0, 0.0))}
    
    green_cmap = LinearSegmentedColormap('GreenCmap', cdict)
    
    colors = green_cmap(np.linspace(0, 1, len(A)))
    for i in range(0,len(A)):
        plotband(A[i],B[i], val="S2", color=colors[i])
    return 0

def figure(ax=None, aspect="equal"):
    """
    Wraps a Matplotlib AxesSubplot or generates a new one. Emulates matplotlib's
    > figure, ax = pyplot.subplots()

    Parameters
    ----------
    ax: AxesSubplot, None
        The matplotlib AxesSubplot to wrap
    scale: float, None
        The scale factor of the ternary plot
    """

    tri_ax = ternplot(ax=ax)
    tri_ax.ax.set_aspect(aspect)
    return tri_ax.get_figure(), tri_ax

def mpl_redraw_callback(event, tax):
    """
    Callback to properly rotate and redraw text labels when the plot is drawn 
    or resized.

    Parameters:
    event: a matplotlib event
        either 'resize_event' or 'draw_event'
    tax: TernaryAxesSubplot
         the TernaryAxesSubplot 
    """

    tax._redraw_labels()
    
class ternplot(object):
    def __init__(self, ax=None):
        
        if ax:
            self.ax = ax
        else:
            _, self.ax = plt.subplots()
        
        self.S1 = (90.0,0.0,0.0)
        self.S2 = (0.0,90.0,0.0)
        self.S3 = (0.0,0.0,90.0)
        
        
        self._labels= dict()
        self._ticks=dict()
        self._to_remove = []
        self._connect_callbacks()
        
        self.boundary()
        self.background_image()
        self.boundary()
        
        self.grid_line(10, val="S1")
        self.grid_line(10, val="S2")
        self.grid_line(10, val="S3")
        self.plot(*self.S1, marker="o", color="black", markersize=20)
        self.plot(*self.S2, marker="o", color="black", markersize=20)
        self.plot(*self.S3, marker="o", color="black", markersize=20)
        
        self.left_axis_label("S2 Dip", fontsize=20.0)
        self.right_axis_label("S1 Dip", fontsize=20.0)
        self.bottom_axis_label("S3 Dip", fontsize=20.0)
        self.annotate("S1", (90,0,0),(90,0,0), color="red")
        self.annotate("S2", (0,90,0),(0,90,0), color="red")
        self.annotate("S3", (0,0,90),(0,0,90), color="red")
        
    def _connect_callbacks(self):
        """
        Connect resize matplotlib callbacks.
        """

        figure = self.get_figure()
        callback = partial(mpl_redraw_callback, tax=self)
        event_names = ('resize_event', 'draw_event')
        for event_name in event_names:
            figure.canvas.mpl_connect(event_name, callback)
            
    def __repr__(self):
        return "FrohlichAxesSubplot: %s" % self.ax.__hash__()
            
    def get_axes(self):
        """
        Return the underlying matplotlib AxesSubplot object
        """

        return self.ax

    def get_figure(self):
        """
        Return the underlying matplotlib figure object.
        """

        ax = self.get_axes()
        return ax.get_figure()
            
    # Title and Axis Labels
    def set_title(self, title, **kwargs):
        """
        Sets the title on the underlying matplotlib AxesSubplot
        """

        ax = self.get_axes()
        ax.set_title(title, **kwargs)
        return
        
    def left_axis_label(self, label, position=None,  rotation=60, offset=7.0,
                        **kwargs):
        """
        Sets the label on the left axis.

        Parameters
        ----------
        ax: Matplotlib AxesSubplot, None
            The subplot to draw on.
        label: String
            The axis label
        position: 3-Tuple of floats, None
            The position of the text label
        rotation: float, 60
            The angle of rotation of the label
        offset: float,
            Used to compute the distance of the label from the axis
        kwargs:project_point
            Any kwargs to pass through to matplotlib.
        """

        if not position:
            position = (-offset, 45., 45.)
        self._labels["left"] = (label, position, rotation, kwargs)

    def right_axis_label(self, label, position=None, rotation=-60, offset=7.0,
                         **kwargs):
        """
        Sets the label on the right axis.

        Parameters
        ----------
        ax: Matplotlib AxesSubplot, None
            The subplot to draw on.
        label: String
            The axis label
        position: 3-Tuple of floats, None
            The position of the text label
        rotation: float, -60
            The angle of rotation of the label
        offset: float,
            Used to compute the distance of the label from the axis
        kwargs:
            Any kwargs to pass through to matplotlib.
        """

        if not position:
            position = (45., 45., -offset)
        self._labels["right"] = (label, position, rotation, kwargs)

    def bottom_axis_label(self, label, position=None, rotation=0, offset=7.0,
                          **kwargs):
        """
        Sets the label on the bottom axis.

        Parameters
        ----------
        ax: Matplotlib AxesSubplot, None
            The subplot to draw on.
        label: String
            The axis label
        position: 3-Tuple of floats, None
            The position of the text label
        rotation: float, 0
            The angle of rotation of the label
        offset: float,
            Used to compute the distance of the label from the axis
        kwargs:
            Any kwargs to pass through to matplotlib.
        """

        if not position:
            position = (45., -offset, 45.)
        self._labels["bottom"] = (label, position, rotation, kwargs)

    def annotate(self, text, position, textpos, **kwargs):
        ax = self.get_axes()
        p = project_point(*position)
        t = project_point(*textpos)
        ax.annotate(text, xy=(p[0], p[1]), xytext=(t[0],t[1]),**kwargs)

    # Boundary and gridlines        
    def boundary(self, linewidth=1.0, color="black"):
        plotline(0., val="S1", linewidth=linewidth, color=color)
        plotline(0., val="S2", linewidth=linewidth, color=color)
        plotline(0., val="S3", linewidth=linewidth, color=color)
        
    def grid_line(self, intervals=10, val="S1", **kwargs):
        for i in range(0,90,intervals, **kwargs):
            plotline(i, val=val, **kwargs)
            
    # ticks
    def clear_matplotlib_ticks(self, axis="both"):
        """
        Clears the default matplotlib ticks.
        """

        ax = self.get_axes()
        ax.set_xticks([])
        ax.set_yticks([])

    def ticks(self, ticks=None, locations=None, multiple=1, axis='blr',
              clockwise=False, axes_colors=None, **kwargs):
        ax = self.get_axes()
        #scale = self.get_scale()
        #lines.ticks(ax, scale, ticks=ticks, locations=locations,
        #            multiple=multiple, clockwise=clockwise, axis=axis, 
        #            axes_colors=axes_colors, **kwargs)   
        
        
    # Matplotlib passthroughs
    def legend(self, *args, **kwargs):
        ax = self.get_axes()
        ax.legend(*args, **kwargs)

    def savefig(self, filename, dpi=200, format=None):
        figure = self.get_figure()
        figure.savefig(filename, format=format, dpi=dpi)

    def show(self):
        plt.show()
        
    def _redraw_labels(self):
        """
        Redraw axis labels, typically after draw or resize events.
        """

        ax = self.get_axes()
        # Remove any previous labels
        for mpl_object in self._to_remove:
            mpl_object.remove()
        self._to_remove = []
        # Redraw the labels with the appropriate angles
        for (label, position, rotation, kwargs) in self._labels.values():
            transform = ax.transAxes
            x, y = project_point(*position)
            # Calculate the new angle.
            position = np.array([x,y])
            new_rotation = ax.transData.transform_angles(np.array((rotation,)), position.reshape((1,2)))[0]
            new_rotation = 0.
            text = ax.text(x, y, label, rotation=rotation, horizontalalignment="center", **kwargs)
            text.set_rotation_mode("anchor")
            self._to_remove.append(text)
        
    def plot(self, S1, S2, S3, **kwargs):
        x, y = project_point(S1,S2,S3)
        plt.plot(x,y, **kwargs)
        
    def background_image(self):
        fig = plt.gcf()
        ax = plt.gca()

        plotred()
        ax.axis("off")
        plt.subplots_adjust(left=0, right=1.0, top=1.0, bottom=0)
        fig.canvas.draw() 
        w, h = fig.canvas.get_width_height()
        img1 = np.frombuffer(fig.canvas.buffer_rgba(), np.uint8).reshape(h, w, -1).copy()

        ax.clear()
        
        plotgreen()
        ax.axis("off")
        plt.subplots_adjust(left=0, right=1.0, top=1.0, bottom=0)
        fig.canvas.draw() 
        w, h = fig.canvas.get_width_height()
        img2 = np.frombuffer(fig.canvas.buffer_rgba(), np.uint8).reshape(h, w, -1).copy()
        ax.clear()      
        
        plotblue()
        ax.axis("off")
        plt.subplots_adjust(left=0, right=1.0, top=1.0, bottom=0)
        fig.canvas.draw() 
        w, h = fig.canvas.get_width_height()
        img3 = np.frombuffer(fig.canvas.buffer_rgba(), np.uint8).reshape(h, w, -1).copy()
        ax.clear() 
        
        img1[img1[:, :, -1] == 0] = 0
        img2[img2[:, :, -1] == 0] = 0
        img3[img3[:, :, -1] == 0] = 0

        fig.clf()
        img = np.maximum(np.maximum(img1,img2),img3)
        
        plt.imshow(img, extent=[-1.5,1.5,-1.0,2.0], aspect="equal", interpolation="none")
        plt.axis("off")
